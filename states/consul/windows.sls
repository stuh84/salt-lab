consul_user:
  user.present:
    - name: Consul
    - groups:
      - Users
    - fullname: Hashicorp Consul
    - enforce_password: False

consul_base_dir:
  file.directory:
  - name: 'C:\consul'
  - win_owner: Consul
  - win_perms: {'Users': {'perms': 'full_control', 'applies_to': 'this_folder_only'}}
  - makedirs: True

{% if not salt['file.file_exists']('c:\consul\consul.exe') %}

retrieve_consul:
  cmd.run:
    - name: '[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"; Invoke-WebRequest -uri https://releases.hashicorp.com/consul/1.7.2/consul_1.7.2_windows_amd64.zip -OutFile c:\consul\consul.zip'
    - shell: powershell

extract_consul:
  archive.extracted:
    - name: c:\consul
    - enforce_toplevel: false
    - source: c:\consul\consul.zip

remove_consul_zip:
  file.absent:
    - name: c:\consul\consul.zip
{% endif %}

consul_conf_dir:
  file.directory:
  - name: 'C:\consul\conf'
  - win_owner: Consul
  - win_perms: {'Users': {'perms': 'full_control', 'applies_to': 'this_folder_only'}}
  - makedirs: True

consul_data_dir:
  file.directory:
  - name: 'C:\consul\data'
  - win_owner: Consul
  - win_perms: {'Users': {'perms': 'full_control', 'applies_to': 'this_folder_only'}}
  - makedirs: True

c:\consul\conf\consul.hcl:
  file.managed:
    - source: salt://consul/client/files/consul.hcl.j2
    - win_owner: consul
    - template: jinja

{% if salt['service.available']('consul') %}
modify_consul_service:
  module.run:
    - name: service.modify
    - m_name: consul
    - bin_path: 'c:\consul\consul.exe'
    - display_name: 'HashiCorp Consul Client Agent'
    - exe_args: 'agent -config-dir=c:\consul\conf'
    - start_type: auto
{% else %}
create_consul_service:
  module.run:
    - name: service.create
    - m_name: consul
    - bin_path: 'c:\consul\consul.exe'
    - display_name: 'HashiCorp Consul Client Agent'
    - exe_args: 'agent -config-dir=c:\consul\conf'
    - start_type: auto
{% endif %}

running_consul_service:
  service.running:
    - name: consul

{% if pillar['consul'] is defined %}
{% if pillar['consul']['prometheus_services'] is defined %}
{% for service in pillar['consul']['prometheus_services'] %}
c:\consul\conf\{{ service }}.hcl:
  file.managed:
    - source: salt://consul/services/files/{{ service }}.hcl
    - win_owner: consul
    - template: jinja

consul_reload_{{ service }}:
  cmd.run:
    - name: c:\consul\consul.exe reload
    - shell: powershell
    - watch:
      - file: c:\consul\conf\{{ service }}.hcl
{% endfor %}
{% endif %}
{% endif %}
