consul_binary:
  archive.extracted:
    - name: /usr/local/bin 
    - source: https://releases.hashicorp.com/consul/1.7.3/consul_1.7.3_darwin_amd64.zip 
    - source_hash: sha256=813eab12ae5c1b815c293c0453d1658dc34d123ac40f3b20c4b12258e0b1034c 
    - enforce_toplevel: false
    - user: root
    - group: wheel
    - if_missing: /usr/local/bin/consul

consul_user:
  user.present:
    - name: consul
    - fullname: Consul
    - shell: /bin/false
    - home: /etc/consul.d

consul_group:
  group.present:
    - name: consul

/opt/consul:
  file.directory:
    - user: consul
    - group: consul
    - mode: 755
    - makedirs: True

/etc/consul.d:
  file.directory:
    - user: consul
    - group: consul
    - mode: 755
    - makedirs: True

/etc/consul.d/consul.hcl:
  file.managed:
{% if pillar['consul'] is defined %}
{% if pillar['consul']['server'] is defined %}
    - source: salt://consul/server/files/consul.hcl.j2
{% else %}
    - source: salt://consul/client/files/consul.hcl.j2
{% endif %}
{% endif %}
    - user: consul
    - group: consul
    - mode: 0640
    - template: jinja

/Library/LaunchDaemons/com.consul.consul.plist:
  file.managed:
    - source: salt://consul/client/files/consul-launchdaemon
    - user: root
    - group: wheel
    - mode: 644

consul_launchctl_load:
   cmd.run:
    - name: launchctl load /Library/LaunchDaemons/com.consul.consul.plist
    - watch:
       - file: /Library/LaunchDaemons/com.consul.consul.plist
    

consul_service:
  cmd.run:
    - name: launchctl stop com.consul.consul; launchctl start com.consul.consul
    - watch:
      - file: /etc/consul.d/consul.hcl

{% if pillar['consul'] is defined %}
{% if pillar['consul']['prometheus_services'] is defined %}
{% for service in pillar['consul']['prometheus_services'] %}
/etc/consul.d/{{ service }}.hcl:
  file.managed:
    - source: salt://consul/services/files/{{ service }}.hcl
    - user: consul
    - group: consul
    - mode: 0640
    - template: jinja

consul_reload_{{ service }}:
  cmd.run:
    - name: /usr/local/bin/consul reload
    - watch:
      - file: /etc/consul.d/{{ service }}.hcl
{% endfor %}
{% endif %}
{% endif %}
