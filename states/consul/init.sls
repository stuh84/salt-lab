consul_binary:
  archive.extracted:
    - name: /usr/local/bin 
    - source: https://releases.hashicorp.com/consul/1.8.1/consul_1.8.1_linux_amd64.zip 
    - source_hash: sha256=728f5bbccdcecc3d0a569065eb8d74a2bcd74abb7038a8bb20f5e289ee8c2a56
    - enforce_toplevel: false
    - user: root
    - group: root
    - if_missing: /usr/local/bin/consul

consul_user:
  user.present:
    - name: consul
    - fullname: Consul
    - shell: /bin/false
    - home: /etc/consul.d

consul_group:
  group.present:
    - name: consul

/etc/systemd/system/consul.service:
  file.managed:
{% if pillar['consul'] is defined %}
{% if 'server' in pillar['consul'] %}
    - source: salt://consul/server/files/consul.service.j2
{% else %}
    - source: salt://consul/client/files/consul.service.j2
{% endif %}
{% endif %}
    - user: root
    - group: root
    - mode: 0644
    - template: jinja

/opt/consul:
  file.directory:
    - user: consul
    - group: consul
    - mode: 755
    - makedirs: True

/etc/consul.d:
  file.directory:
    - user: consul
    - group: consul
    - mode: 755
    - makedirs: True

/etc/consul.d/consul.hcl:
  file.managed:
{% if pillar['consul'] is defined %}
{% if pillar['consul']['server'] is defined %}
    - source: salt://consul/server/files/consul.hcl.j2
{% else %}
    - source: salt://consul/client/files/consul.hcl.j2
{% endif %}
{% endif %}
    - user: consul
    - group: consul
    - mode: 0640
    - template: jinja

consul_service:
  service.running:
  - name: consul
  - enable: True
  - reload: True
  - watch:
    - file: /etc/consul.d/consul.hcl

{% if pillar['consul'] is defined %}
{% if pillar['consul']['prometheus_services'] is defined %}
{% for service in pillar['consul']['prometheus_services'] %}
/etc/consul.d/{{ service }}.hcl:
  file.managed:
    - source: salt://consul/services/files/{{ service }}.hcl
    - user: consul
    - group: consul
    - mode: 0640
    - template: jinja

consul_reload_{{ service }}:
  cmd.run:
    - name: consul reload
    - watch:
      - file: /etc/consul.d/{{ service }}.hcl
{% endfor %}
{% endif %}
{% endif %}
