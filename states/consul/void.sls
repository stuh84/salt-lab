consul_package:
  pkg.installed:
  - pkgs:
    - consul

consul_user:
  user.present:
    - name: consul
    - fullname: Consul
    - shell: /bin/false
    - home: /etc/consul.d

consul_group:
  group.present:
    - name: consul

/opt/consul:
  file.directory:
    - user: consul
    - group: consul
    - mode: 755
    - makedirs: True

/etc/consul.d:
  file.directory:
    - user: consul
    - group: consul
    - mode: 755
    - makedirs: True

/run/runit/supervise.consul:
  file.directory:
    - user: root
    - group: root
    - mode: 0700
    - makedirs: True

/etc/sv/consul:
  file.directory:
    - user: root
    - group: root
    - mode: 0700
    - makedirs: True
  
/etc/sv/consul/run:
  file.managed:
    - source: salt://consul/client/files/consul-runit
    - user: root
    - group: root
    - mode: 0755

/etc/sv/consul/supervise:
  file.symlink:
   - target: /run/runit/supervise.consul
   - user: root
   - group: root
   - mode: 0777

/var/service/consul:
  file.symlink:
   - target: /etc/sv/consul
   - user: root
   - group: root
   - mode: 0777

/etc/consul.d/consul.hcl:
  file.managed:
{% if pillar['consul'] is defined %}
{% if pillar['consul']['server'] is defined %}
    - source: salt://consul/server/files/consul.hcl.j2
{% else %}
    - source: salt://consul/client/files/consul.hcl.j2
{% endif %}
{% endif %}
    - user: consul
    - group: consul
    - mode: 0640
    - template: jinja

consul_service:
  service.running:
  - name: consul
  - enable: True
  - watch:
    - file: /etc/consul.d/consul.hcl

{% if pillar['consul'] is defined %}
{% if pillar['consul']['prometheus_services'] is defined %}
{% for service in pillar['consul']['prometheus_services'] %}
/etc/consul.d/{{ service }}.hcl:
  file.managed:
    - source: salt://consul/services/files/{{ service }}.hcl
    - user: consul
    - group: consul
    - mode: 0640
    - template: jinja

consul_reload_{{ service }}:
  cmd.run:
    - name: consul reload
    - watch:
      - file: /etc/consul.d/{{ service }}.hcl
{% endfor %}
{% endif %}
{% endif %}
