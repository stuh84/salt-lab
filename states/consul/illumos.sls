consul_binary:
  archive.extracted:
    - name: /usr/bin
    - source: https://releases.hashicorp.com/consul/1.7.3/consul_1.7.3_solaris_amd64.zip 
    - source_hash: sha256=af49c5ff0639977d1efc9e5ef30277842c6bab90f53e4758b22b18e224e14bb1
    - enforce_toplevel: false
    - user: root
    - group: root
    - if_missing: /usr/bin/consul

consul_user:
  user.present:
    - name: consul
    - fullname: Consul
    - shell: /bin/false
    - home: /etc/consul.d

consul_group:
  group.present:
    - name: consul

/opt/consul:
  file.directory:
    - user: consul
    - group: consul
    - mode: 755
    - makedirs: True

/opt/local/etc/consul.d:
  file.directory:
    - user: consul
    - group: consul
    - mode: 755
    - makedirs: True

/lib/svc/manifest/system/consul.xml:
  file.managed:
    - source: salt://consul/client/files/consul-svc-manifest
    - user: root
    - group: sys
    - mode: 0640

consul_import_svc:
  cmd.run:
    - name: svccfg import /lib/svc/manifest/system/consul.xml
    - watch:
       - file: /lib/svc/manifest/system/consul.xml

/opt/local/etc/consul.d/consul.hcl:
  file.managed:
{% if pillar['consul'] is defined %}
{% if pillar['consul']['server'] is defined %}
    - source: salt://consul/server/files/consul.hcl.j2
{% else %}
    - source: salt://consul/client/files/consul.hcl.j2
{% endif %}
{% endif %}
    - user: consul
    - group: consul
    - mode: 0640
    - template: jinja

consul_service:
  service.running:
    - name: consul
    - enable: True
    - reload: True
    - watch:
      - file: /opt/local/etc/consul.d/consul.hcl

{% if pillar['consul'] is defined %}
{% if pillar['consul']['prometheus_services'] is defined %}
{% for service in pillar['consul']['prometheus_services'] %}
/opt/local/etc/consul.d/{{ service }}.hcl:
  file.managed:
    - source: salt://consul/services/files/{{ service }}.hcl
    - user: consul
    - group: consul
    - mode: 0640
    - template: jinja

consul_reload_{{ service }}:
  cmd.run:
    - name: consul reload
    - watch:
      - file: /opt/local/etc/consul.d/{{ service }}.hcl
{% endfor %}
{% endif %}
{% endif %}
