gstat_exporter_deps:
  pkg.installed:
    - pkgs:
      - py37-pip
      - py37-setuptools
      - py37-virtualenv
      - git

gstat_git_repo:
  git.cloned:
    - name: https://github.com/tykling/gstat_exporter
    - target: /usr/local/gstat_exporter

gstat_requirements:
  virtualenv.managed:
    - name: /usr/local/gstat_exporter/venv 
    - requirements: /usr/local/gstat_exporter/requirements.txt

/usr/local/etc/rc.d/gstat_exporter:
  file.managed:
    - source: salt://exporters/gstat_exporter/files/gstat_exporter-rcd
    - mode: 0755

gstat_exporter:
  service.running:
    - enable: true
