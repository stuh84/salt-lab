node_exporter_binary:
  file.managed:
    - name: /usr/bin/node_exporter
    - source: salt://exporters/node_exporter/files/node_exporter_illumos
    - user: root
    - group: bin
    - mode: 0755
    
node_exporter_user:
  user.present:
    - name: node_exporter
    - fullname: Node Exporter
    - shell: /bin/false

node_exporter_group:
  group.present:
    - name: node_exporter

/opt/prometheus/exporters/dist/textfile:
  file.directory:
    - user: node_exporter
    - group: node_exporter
    - mode: 755
    - makedirs: True

/lib/svc/manifest/system/node_exporter.xml:
  file.managed:
    - source: salt://exporters/node_exporter/files/node_exporter-svc-manifest
    - user: root
    - group: sys
    - mode: 0640

node_exporter_import_svc:
  cmd.run:
    - name: svccfg import /lib/svc/manifest/system/node_exporter.xml
    - watch:
       - file: /lib/svc/manifest/system/node_exporter.xml

node_exporter_service:
  service.running:
    - name: node_exporter
    - enable: True
    - reload: True
