{% if not salt['file.file_exists']('/usr/local/bin/node_exporter') %}
extract_node_exporter:
  archive.extracted:
    - name: /usr/local/bin 
    - enforce_toplevel: false
    - source: https://github.com/prometheus/node_exporter/releases/download/v1.0.0/node_exporter-1.0.0.darwin-amd64.tar.gz
    - source_hash: sha256=68eec397b0b88767508aab9ec5214070b5877daef33fb94b1377aa245bb90d01
    - archive_format: tar
    - user: root
    - group: wheel

move_node_exporter:
  file.rename:
    - name: /usr/local/bin/node_exporter
    - source: /usr/local/bin/node_exporter-1.0.0.darwin-amd64/node_exporter

delete_node_exporter_dir:
  file.absent:
    - name: /usr/local/bin/node_exporter-1.0.0.darwin-amd64/

delete_node_exporter_files:
  file.absent:
    - name: /usr/local/bin/node_exporter-1.0.0.darwin-amd64.tar.gz
{% endif %}

node_exporter_user:
  user.present:
    - name: node_exporter
    - fullname: Node Exporter
    - shell: /bin/false

node_exporter_group:
  group.present:
    - name: node_exporter

/opt/prometheus/exporters/dist/textfile:
  file.directory:
    - user: node_exporter
    - group: node_exporter
    - mode: 755
    - makedirs: True

/Library/LaunchDaemons/com.prometheus.node_exporter.plist:
  file.managed:
    - source: salt://exporters/node_exporter/files/node_exporter-launchdaemon
    - user: root
    - group: wheel
    - mode: 644

node_exporter_launchctl_load:
   cmd.run:
    - name: launchctl load /Library/LaunchDaemons/com.prometheus.node_exporter.plist
    - watch:
       - file: /Library/LaunchDaemons/com.prometheus.node_exporter.plist
    
node_exporter_service:
  cmd.run:
    - name: launchctl stop com.prometheus.node_exporter; launchctl start com.prometheus.node_exporter
