{% if not salt['file.file_exists']('/usr/local/bin/node_exporter') %}

retrieve_node_exporter:
  cmd.run:
    - name: wget -O /tmp/node_exporter.tar.gz https://github.com/prometheus/node_exporter/releases/download/v0.18.1/node_exporter-0.18.1.linux-amd64.tar.gz 

extract_node_exporter:
  archive.extracted:
    - name: /tmp
    - enforce_toplevel: false
    - source: /tmp/node_exporter.tar.gz
    - archive_format: tar
    - user: root
    - group: root

move_node_exporter:
  file.rename:
    - name: /usr/local/bin/node_exporter
    - source: /tmp/node_exporter-0.18.1.linux-amd64/node_exporter

delete_node_exporter_dir:
  file.absent:
    - name: /tmp/node_exporter-0.18.1.linux-amd64

delete_node_exporter_files:
  file.absent:
    - name: /tmp/node_exporter.tar.gz
{% endif %}

node_exporter_user:
  cmd.run:
    - name: adduser -H -D -s /bin/false node_exporter 

/opt/prometheus/exporters/dist/textfile:
  file.directory:
    - user: node_exporter
    - group: node_exporter
    - mode: 755
    - makedirs: True

/var/log/node_exporter:
  file.directory:
    - user: node_exporter
    - group: node_exporter
    - mode: 755
    - makedirs: True

/etc/init.d/node_exporter:
  file.managed:
    - source: salt://exporters/node_exporter/files/node_exporter.openrc
    - dest: /etc/init.d/node_exporter
    - mode: '0755'
    - user: root
    - group: root

node_exporter_service:
  service.running:
    - name: node_exporter
    - enable: true
    - state: restarted
    - watch:
      - file: /etc/init.d/node_exporter
