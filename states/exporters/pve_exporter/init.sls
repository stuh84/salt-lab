install_pve_exporter:
  pip.installed:
    - name: prometheus-pve-exporter
    - user: root
    - reload_modules: True

prometheus_user:
  user.present:
    - name: prometheus
    - fullname: Prometheus
    - shell: /bin/false

/etc/prometheus/exporters:
  file.directory:
    - user: prometheus
    - group: prometheus
    - mode: 755
    - makedirs: True

/etc/prometheus/exporters/pve.yaml:
  file.managed:
    - source: salt://exporters/pve_exporter/files/pve.yaml.j2
    - user: prometheus
    - group: prometheus
    - mode: 0644
    - template: jinja

/etc/systemd/system/pve_exporter.service:
  file.managed:
    - source: salt://exporters/pve_exporter/files/pve_exporter.service.j2
    - user: root
    - group: root
    - mode: 0644
    - template: jinja

pve_exporter_service_reload:
  cmd.run:
    - name: systemctl daemon-reload
    - watch:
      - file: /etc/systemd/system/pve_exporter.service

pve_exporter_service:
  service.running:
  - name: pve_exporter
  - enable: True
