windows_exporter_dir:
  file.directory:
  - name: 'C:\exporter\windows_exporter'
  - win_owner: exporter
  - win_perms: {'Users': {'perms': 'full_control', 'applies_to': 'this_folder_only'}}
  - makedirs: True

{% if not salt['file.file_exists']('c:\exporter\windows_exporter\windows_exporter.msi') %}

retrieve_windows_exporter:
  cmd.run:
    - name: '[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"; Invoke-WebRequest -uri https://github.com/prometheus-community/windows_exporter/releases/download/v0.13.0/windows_exporter-0.13.0-amd64.msi -OutFile c:\exporter\windows_exporter\windows_exporter.msi'
    - shell: powershell
{% endif %}

{% if not salt['service.available']('windows_exporter') %}
windows_exporter_install:
  cmd.run:
    - name: msiexec /i C:\exporter\windows_exporter\windows_exporter.msi /quiet ENABLED_COLLECTORS="cpu,cs,logical_disk,memory,net,os,process,service,system,textfile" TEXTFILE_DIR="C:\exporter\textfile"
    - shell: powershell
{% endif %}
