exporter_user:
  user.present:
    - name: exporter
    - groups:
      - Users
    - fullname: Prometheus Exporter User 
    - enforce_password: False

exporter_base_dir:
  file.directory:
  - name: 'C:\exporter'
  - win_owner: exporter
  - win_perms: {'Users': {'perms': 'full_control', 'applies_to': 'this_folder_only'}}
  - makedirs: True

exporter_textfile_dir:
  file.directory:
  - name: 'C:\exporter\textfile'
  - win_owner: exporter
  - win_perms: {'Users': {'perms': 'full_control', 'applies_to': 'this_folder_only'}}
  - makedirs: True

