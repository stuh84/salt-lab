# My Salt Lab 

This repository contains the Salt States and Pillars for my Salt-based posts on my [blog](https://yetiops.net)


# Deploying SaltStack, Consul and Prometheus Exporters with Salt

## Completed

* [Linux](https://yetiops.net/posts/prometheus-consul-saltstack-part-1-linux/)

## Work-in-Progress

* Windows 
* OpenBSD 
* FreeBSD
* MacOS
* illumos 
